const array = require('./2-arrays-logins.cjs')

const arrayIpAddress = array.reduce((acc, {ip_address}) => {
  const ipAddress = ip_address.split('.').map(num => parseInt(num));
  acc.push(ipAddress);
  return acc;
}, []);

console.log(arrayIpAddress);