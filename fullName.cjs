const array = require('./2-arrays-logins.cjs')

const newArray = array.reduce((acc, obj) => {
    obj["full_name"] = `${obj.first_name} ${obj.last_name}`;
    acc.push(obj);
    return acc;
  }, []);
  
  console.log(newArray);