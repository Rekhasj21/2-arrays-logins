const array = require('./2-arrays-logins.cjs')

const arrayIpAddress = array.map(({ ip_address }) => ip_address.split('.').map(num => parseInt(num)));

const result = arrayIpAddress.reduce((total, ip) => total + ip[3], 0);

console.log(result);